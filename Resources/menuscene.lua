

local visibleSize = CCDirector:sharedDirector():getVisibleSize()
local origin = CCDirector:sharedDirector():getVisibleOrigin()

function createMenuScene()
    local layerMenuScene = CCLayer:create()

    local background = CCSprite:create("background_menu.png")
    background:setPosition(ccp(visibleSize.width/2, visibleSize.height/2))
    layerMenuScene:addChild(background)

    local logo = CCSprite:create("logo.png")
    logo:setPosition(ccp(visibleSize.width/2, visibleSize.height * 0.7))
    layerMenuScene:addChild(logo)

    local menuStartButton = CCMenuItemFont:create("게임시작")
    menuStartButton:setFontSizeObj(60)
    menuStartButton:setFontNameObj("fonts/Moebius.ttf")
    menuStartButton:setPosition(ccp(visibleSize.width/2, visibleSize.height * 0.2))
    menuStartButton:registerScriptTapHandler( 
    	function() 
    		print("start button clicked") 
    		require "gamescene"
    		local sceneGame = CCScene:create()
			sceneGame:addChild(createGameScene())
			CCDirector:sharedDirector():replaceScene(CCTransitionJumpZoom:create(1, sceneGame))
    	end )

    local menuDeveloperBriefButton = CCMenuItemFont:create("개발자의 한마디")
    menuDeveloperBriefButton:setFontSizeObj(60)
    menuDeveloperBriefButton:setFontNameObj("fonts/Moebius.ttf")
    menuDeveloperBriefButton:setPosition(ccp(visibleSize.width/2, visibleSize.height * 0.1))
    menuDeveloperBriefButton:registerScriptTapHandler( 
    	function() 
    		print("developer button clicked")

    		local mybrief = CCSprite:create("mybrief.png")
    		local h = mybrief:getContentSize().height/2
    		mybrief:setPosition(ccp(visibleSize.width/2, -h))

    		local actions = CCArray:create()
    		actions:addObject( CCMoveBy:create(0.5, ccp(0, h + visibleSize.height/2)) )
    		actions:addObject( CCDelayTime:create(5) )
    		actions:addObject( CCMoveBy:create(0.5, ccp(0, -(h + visibleSize.height/2))) )
    		actions:addObject( CCCallFunc:create( function() layerMenuScene:removeChild(mybrief, true) end ) )

    		local seq = CCSequence:create(actions)
    		mybrief:runAction( seq )
    		layerMenuScene:addChild(mybrief)
		end )

    local items = CCArray:create()
    items:addObject( menuStartButton )
    items:addObject( menuDeveloperBriefButton )

    local menu = CCMenu:createWithArray(items)
    -- local itemWidth = menuStartButton:getContentSize().width
    -- local itemHeight = menuStartButton:getContentSize().height
    menu:setPosition(origin.x, origin.y)
    layerMenuScene:addChild(menu)

    local function onTouchBegan(x, y)
        -- CCTOUCHBEGAN event must return true
        return true
    end

    local function onTouchMoved(x, y)

    end

    local function onTouchEnded(x, y)
        
    end

    local function onTouch(eventType, x, y)
        if eventType == "began" then   
            return onTouchBegan(x, y)
        elseif eventType == "moved" then
            return onTouchMoved(x, y)
        else
            return onTouchEnded(x, y)
        end
    end

    layerMenuScene:registerScriptTouchHandler(onTouch)
    layerMenuScene:setTouchEnabled(true)

    return layerMenuScene
end

