
local visibleSize = CCDirector:sharedDirector():getVisibleSize()
local origin = CCDirector:sharedDirector():getVisibleOrigin()

local function createSpriteAnimation( imgpath, frameWidth, frameHeight, frameAmount, time )
    local animFrames = CCArray:create()
    local textureExplosion = CCTextureCache:sharedTextureCache():addImage(imgpath)
    local rect = CCRectMake(0, 0, frameWidth, frameHeight)
    for i=0, frameAmount-1 do
        rect.origin.x = i * frameWidth
        local frame = CCSpriteFrame:createWithTexture(textureExplosion, rect)
        animFrames:addObject(frame)
    end
    local animation = CCAnimation:createWithSpriteFrames(animFrames, time/frameAmount)
    local animate = CCAnimate:create(animation)

    return animate
end

local PLAY_TIME = 60
local HP = 10
local SCALE = 0.4

local brokenCount = 0

local function createOverMessageBox() 
    local msg = CCSprite:create("over_box.png")
    msg:setPosition(ccp(visibleSize.width/2, visibleSize.height/2))

    local countLabel = CCLabelTTF:create(tostring(brokenCount), "fonts/Moebius.ttf", 100)
    countLabel:setPosition(ccp(msg:getContentSize().width/2, msg:getContentSize().height * 0.4))
    -- Below method is not supported....;; How can I change font color?
    -- countLabel:enableStroke(ccc3(1,1,1), 3)
    -- countLabel:setFontFillColor(ccc3(0,0,0))
    msg:addChild(countLabel)

    local gotoMenuSceneButton = CCMenuItemImage:create("button_menu_normal.png", "button_menu_normal.png")
    gotoMenuSceneButton:setPosition(ccp(visibleSize.width/2 - 100, 0))
    gotoMenuSceneButton:setScale(2)
    gotoMenuSceneButton:registerScriptTapHandler( 
        function() 
            require "menuscene"
            brokenCount = 0
            local sceneGame = CCScene:create()
            sceneGame:addChild(createMenuScene())
            CCDirector:sharedDirector():replaceScene(CCTransitionJumpZoom:create(1, sceneGame))
        end )

    local retryButton = CCMenuItemImage:create("button_retry_normal.png", "button_retry_normal.png")
    retryButton:setPosition(ccp(visibleSize.width/2 + 100, 0))
    retryButton:setScale(2)
    retryButton:registerScriptTapHandler( 
        function() 
            -- Is it right way?
            brokenCount = 0
            local sceneGame = CCScene:create()
            sceneGame:addChild(createGameScene())
            CCDirector:sharedDirector():replaceScene(CCTransitionJumpZoom:create(1, sceneGame))
        end )

    local items = CCArray:create()
    items:addObject( gotoMenuSceneButton )
    items:addObject( retryButton )

    local menu = CCMenu:createWithArray(items)
    menu:setPosition(origin.x, origin.y)
    msg:addChild(menu)

    return msg
end

-- create farm
function createGameScene()
    local layerGameScene = CCLayer:create()

    local background = CCSprite:create("background.png")
    background:setPosition(ccp(visibleSize.width/2, visibleSize.height/2))
    layerGameScene:addChild(background)

    local timeLabel = CCLabelTTF:create(tostring(PLAY_TIME), "fonts/Moebius.ttf", 80)
    timeLabel:setPosition(ccp(visibleSize.width/2, visibleSize.height*0.9))
    layerGameScene:addChild(timeLabel)
    local time = PLAY_TIME
    local prevTime = time
    local function timeHandler(dt) 
        -- I WANT TO USE COROUTINE ON HERE!!!! (But not enough time :p)
        time = time - dt
        if time <= 0 then
            layerGameScene:unscheduleUpdate()
            layerGameScene:addChild(createOverMessageBox(), 10)            
        end

        local floored = math.floor(time)
        if prevTime ~= floored then
            timeLabel:setString(tostring(floored))
            prevTime = floored
        end
    end
    layerGameScene:scheduleUpdateWithPriorityLua( timeHandler, 1 )

    local bbabbaro = CCSprite:create("bbabbaro.png")
    bbabbaro:setPosition(ccp(visibleSize.width/2, visibleSize.height/2))
    bbabbaro:setScale(SCALE)
    layerGameScene:addChild(bbabbaro)
    bbabbaro.hp = HP;
    
    local function onTouchBegan(x, y)
        -- CCTOUCHBEGAN event must return true
        return true
    end

    local function onTouchMoved(x, y)

    end

    local function onTouchEnded(x, y)
        local bound = bbabbaro:boundingBox()
        if bound:containsPoint(ccp(x,y)) then
            local shaky = CCShaky3D:create(0.1, CCSizeMake(10,10), 10, false)
            bbabbaro:runAction( shaky )

            bbabbaro.hp = bbabbaro.hp - 1
            print( "hp : " .. bbabbaro.hp )
            if bbabbaro.hp == 0 then
                brokenCount = brokenCount + 1 
                local old = bbabbaro

                local actions = CCArray:create()
                    local actionsForSpawn = CCArray:create()
                    actionsForSpawn:addObject(createSpriteAnimation( "explosion.png", 64, 64, 40, 1 ))
                    actionsForSpawn:addObject(CCMoveBy:create(1, ccp(visibleSize.width, visibleSize.height)))
                    actionsForSpawn:addObject(CCRotateBy:create(1, 360))
                    local spawn = CCSpawn:create(actionsForSpawn)
                    actions:addObject(spawn)
                    actions:addObject(CCCallFunc:create( 
                        function() 
                            layerGameScene:removeChild(old, true) 
                        end ))
                local seq = CCSequence:create(actions)
                old:setScale(5)
                old:runAction(seq)

                bbabbaro = CCSprite:create("bbabbaro.png")
                bbabbaro:setPosition(ccp(visibleSize.width/2, visibleSize.height/2))
                bbabbaro:setScale(SCALE)
                layerGameScene:addChild(bbabbaro)
                bbabbaro.hp = HP;
            end            
        end
    end

    local function onTouch(eventType, x, y)
        if eventType == "began" then   
            return onTouchBegan(x, y)
        elseif eventType == "moved" then
            return onTouchMoved(x, y)
        else
            return onTouchEnded(x, y)
        end
    end

    layerGameScene:registerScriptTouchHandler(onTouch)
    layerGameScene:setTouchEnabled(true)

    return layerGameScene
end